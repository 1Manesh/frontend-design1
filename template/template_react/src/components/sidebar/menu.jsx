const Menu = [
  { path: '/dashboard', icon: 'fa fa-th', title: 'Dashboard',
    children: [
      { path: '/dashboard/v1', title: 'Dashboard v1' },
      { path: '/dashboard/v2', title: 'Dashboard v2' },
      { path: '/dashboard/v3', title: 'Dashboard v3' },
      { path: '/dashboard/v4', title: 'Dashboard v4' }
    ]
  },
  { path: '/email', icon: 'fa fa-hdd', title: 'Email', badge: '10',
    children: [
      { path: '/email/inbox', title: 'Inbox' },
      { path: '/email/compose', title: 'Compose' },
      { path: '/email/detail', title: 'Detail' }
    ]
  },

  { path: '/usermanagement', icon: 'fa fa-hdd', title: 'News Letter', badge: '10',
  children: [
    { path: '/newsmanagement/newslist', title: 'Newsletter-list' },
    
  ]
},

   {
    path: '/userdashboard', icon: 'fa fa-hdd', title: 'User Management',
    children: [
      { path: '/userdashboard/userdata', title: 'User List' },

    ]
  },

  {
    path: '/emailmanagement', icon: 'fa fa-hdd', title: 'Email Mangement',
    children: [
      { path: '/emailmanagement/emails', title: 'Email List' },

    ]
  },

  
  
   
 
 
  


  
  { path: '/user', icon: 'fa fa-key', title: 'Login & Register',
    children: [
      { path: '/user/login-v1', title: 'Login' },
      { path: '/user/login-v2', title: 'Login v2' },
      { path: '/user/login-v3', title: 'Login v3' },
      { path: '/user/register-v3', title: 'Register v3' }
    ]
  },
  
]

export default Menu;
