import React from "react";
import ReactDOM from "react-dom";
import MUIDataTable from "mui-datatables";
import FormDialog from "./FormDialog";
function EmailTemplate(){
    
    const columns = ["Name", "LastName", "Country", "Mobile No", "EmailAddress"];

    const data = [
      ["Gabby ", "George", "Minneapolis", 2300000000, "GabbyGeroge@yahoo.com", ],
      ["Aiden", "Lloyd", "Dallas", 55201213213131, "Aiden@yahoo.com"],
      ["Jaden ", "Collins", "Santa Ana", 2778964226, "Jaden@yahoo.com"],
      ["Franky ", "Rees", "St. Petersburg", 227412586932, "Franky@yahoo.com"],
      ["Aaren ", "Rose", "Toledo", 28745698, "Aaren@yahoo.com"],
      [
        "Blake ",
        "Duncan",
        "San Diego",
        6585552222,
        "Blake@yahoo.com"
      ],
      ["Frankie", "Duncan", "Jacksonville", 71963258, "Frankie@yahoo.com"],
      ["Lane", " Wilson", "Omaha", 19741569, "Lane@yahoo.com"],
      ["Robin ", "Duncan", "Los Angeles", 20741239, "Robin@yahoo.com"],
      ["Mel", " Brooks", "Oklahoma City", 3789632147, "Brooks@yahoo.com"],
      ["Harper ", "White", "Pittsburgh", 5274589631, "White@yahoo.com"],
      ["Kris ", "Humphrey", "Laredo", 30785412, "Humphrey@yahoo.com"],
      ["Frankie ", "Long", "Austin", 312369854, "Long@yahoo.com"],
      ["Brynn ", "Robbins", "Norfolk", 22741258, "Robbins@yahoo.com"],
      ["Justice ", "Mann", "Chicago", 249632587, "Justice @yahoo.com"],
      [
        "Addison",
        " Navarro",
        "New York",
        507865412,
        "Addison@yahoo.com"
      ],
      ["Jesse", " Welch", "Seattle", 288523697, "Jesse@yahoo.com"],
      ["Eli", " Mejia", "Long Beach", 65745698, "Elie@yahoo.com"],
      ["Gene ", "Leblanc", "Hartford", 348523697, "G@yahoo.com"],
      ["Danny ", "Leon", "Newark", 607412369, "Leon@yahoo.com"],
      ["Lane ", "Lee", "Cincinnati", 527456982, "Lanee@yahoo.com"],
      ["Jesse ", "Hall", "Baltimore", 448569321, "Hall@yahoo.com"],
      ["Danni", " Hudson", "Tampa", 37789654, "Jesse@yahoo.com"],
      ["Terry ", "Macdonald", "Miami", 393114569, "Macdonald@yahoo.com"],
      ["Justice ", "Mccarthy", "Tucson", 26963258, "Justicee@yahoo.com"],
      ["Silver ", "Carey", "Memphis", 474556216, "Carey@yahoo.com"],
      ["Franky ", "Miles", "Buffalo", 499658741, "Franky@yahoo.com"],
      ["Glen ", "Nixon", "Arlington", 4423698547, "Nixoneroge@yahoo.com"],
      [
        "Gabby ",
        "Strickland",
        "Scottsdale",
        26789654123,
        "Strickland",
        "@yahoo.com"
      ],
      ["Mason Ray", "ronty", "San Francisco", 3914785993, "rontyy@yahoo.com"]
    ];

    const options = {
      filterType: "dropdown",
      responsive: "scroll"
    };
   

    return (
        <>
        <FormDialog/>
        
      <MUIDataTable
        title={"User List"}
        data={data}
        columns={columns}
        options={options}
      />
      </>
      
    );
  }
  export default EmailTemplate
