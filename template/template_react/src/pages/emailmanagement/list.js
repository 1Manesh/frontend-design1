import React, { Component } from 'react';
import Modal from './modal.js';

class List extends Component {
  constructor(props) {
    super(props);

    this.replaceModalItem = this.replaceModalItem.bind(this);
    this.saveModalDetails = this.saveModalDetails.bind(this);
    this.state = {
      requiredItem: 0,
      brochure: [
        {
          Email: "arsalanahmed1199agmail.com",
         
        }, {
          Email: "manesh@gmail.com",
         
        }, {
          Email: "faizan@gmail.com",
         
        },
        {
          Email: "zomamustafa1199agmail.com",
         
        },
        {
          Email: "sarfarazagmail.com",
         
        },
        {
          Email: "aliagmail.com",
         
        },
        {
          Email: "nomanagmail.com",
         
        },
        {
          Email: "umeragmail.com",
         
        },
        {
          Email: "omaima.com",
         
        },
        {
          Email: "aliza2004com",
         
        },
        {
          Email: "usman715112@.com",
         
        },
        {
          Email: "sawaizmustafaagmail.com",
         
        },
        {
          Email: "maneshbgmail.com",
         
        },
        {
          Email: "akash789645@.com",
         
        },
        {
          Email: "ahmedarsalangmail.com",
         
        },
        {
          Email: "wajahatabbasigmail.com",
         
        },
      ]
    }
  }

  replaceModalItem(index) {
    this.setState({
      requiredItem: index
    });
  }

  saveModalDetails(item) {
    const requiredItem = this.state.requiredItem;
    let tempbrochure = this.state.brochure;
    tempbrochure[requiredItem] = item;
    this.setState({ brochure: tempbrochure });
  }

  deleteItem(index) {
    let tempBrochure = this.state.brochure;
    tempBrochure.splice(index, 1);
    this.setState({ brochure: tempBrochure });
  }

  render() {    
    const brochure = this.state.brochure.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.Email}</td>
          <td>{" "} - {" "}</td>
         
          <td>
            <button className="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
              onClick={() => this.replaceModalItem(index)}>edit</button> {" "}
            <button className="btn btn-danger" onClick={() => this.deleteItem(index)}>remove</button>
          </td>
        </tr>
      )
    });
    
    const requiredItem = this.state.requiredItem;
    let modalData = this.state.brochure[requiredItem];
    return (
      <div>
        <div style={{ textAlign: "center" }}>
          <h1>Email Management</h1>
          
        </div>
        <table className="table table-striped">
          <tbody>
            {brochure}
          </tbody>
        </table>
        <Modal
          Email={modalData.Email}
         
          saveModalDetails={this.saveModalDetails}
        />
      </div>
    );
  }
}

export default List;
