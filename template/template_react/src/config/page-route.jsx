import React from 'react';
import { Redirect } from 'react-router';

import DashboardV1 from './../pages/dashboard/dashboard-v1.js';
import DashboardV2 from './../pages/dashboard/dashboard-v2.js';
import DashboardV3 from './../pages/dashboard/dashboard-v3.js';
import DashboardV4 from './../pages/dashboard/dashboard-v4.js';
import EmailInbox from './../pages/email/email-inbox.js';
import EmailCompose from './../pages/email/email-compose.js';
import EmailDetail from './../pages/email/email-detail.js';


import UserList from '../pages/usermanagement/userlist.js';

import UserData from '../pages/userdashboard/userdata.js'

import Emails from '../pages/emailmanagement/emails.js'










import LoginV1 from './../pages/user/login-v1.js';
import LoginV2 from './../pages/user/login-v2.js';
import LoginV3 from './../pages/user/login-v3.js';
import RegisterV3 from './../pages/user/register-v3.js';


const routes = [
  {
    path: '/',
    exact: true,
    component: () => <Redirect to='/dashboard/v2'/>
  },
  {
    path: '/dashboard/v1',
    exact: true,
    title: 'Dashboard V1',
    component: () => <DashboardV1 />
  },
  {
    path: '/dashboard/v2',
    title: 'Dashboard V2',
    component: () => <DashboardV2 />
  },
  {
    path: '/dashboard/v3',
    title: 'Dashboard V3',
    component: () => <DashboardV3 />
  },

  {
    path: '/dashboard/v4',
    title: 'Dashboard V4',
    component: () => <DashboardV4 />
  },

  {
    path: '/newsmanagement/newslist',
    title: 'User List',
    component: () => <UserList />
  },
  
   {
     path: '/userdashboard/userdata',
    title: 'User Data',
     component: () => <UserData />
 },

 {
  path: '/emailmanagement/emails',
 title: 'Emails',
  component: () => <Emails />
},


 


  
  
{
    path: '/email/inbox',
    title: 'Email Inbox',
    component: () => <EmailInbox />,
  },
  {
    path: '/email/compose',
    title: 'Email Compose',
    component: () => <EmailCompose />,
  },
  {
    path: '/email/detail',
    title: 'Email Detail',
    component: () => <EmailDetail />,
  },
 
  
   
    {
    path: '/user/login-v1',
    title: 'Login',
    component: () => <LoginV1 />,
  },
  {
    path: '/user/login-v2',
    title: 'Login v2',
    component: () => <LoginV2 />,
  },
  {
    path: '/user/login-v3',
    title: 'Login v3',
    component: () => <LoginV3 />,
  },
  {
    path: '/user/register-v3',
    title: 'Register v3',
    component: () => <RegisterV3 />,
  },
 
];


export default routes;